class SalesItem < ActiveRecord::Base
  belongs_to :category

  def self.countitems   #to call into view, define as class model with "self.methodname"
    @salescount = SalesItem.count
  end

  def self.averageprice
    @salesaverage = SalesItem.average(:price)
  end

end
